<?php

/**
 * @title 亚马逊应收账款导出--可合并单元格
 * @author 李献涛
 * @date 2022/7/20
 * @time 15:54
 */

namespace app\bi\service\amazon_receivables_two;

use app\bi\helper\CommonHelper;
use app\bi\helper\OrderHelper;
use app\bi\model\DimAccountShopModel;
use app\bi\model\DimChannelModel;
use app\bi\model\FactAmazonReceivablesTwoModel;
use app\bi\model\FactDayAccountSummaryModel;
use app\common\service\Common;
use app\common\service\AmazonReceivablesExcel;
use app\common\service\Filter;
use app\common\service\OrderType;
use app\finance\service\AmazonFinaceExternal;
use app\index\service\AccountStatusService;
use app\index\service\Department;
use app\report\model\ReportExportFiles;
use think\Config;
use think\Db;
use think\Exception;
use think\exception\DbException;

class ExportNewService
{
    use CommonTrait;

    protected $accountFrozenData = [];
    protected $dimAccountShopData = [];
    protected $accountUserMap = [];
    protected $userData = [];
    protected $salesCompanyData = [];

    /**
     * 执行导出
     * @param array $chooseFields
     * @param array $file
     * @param array $params
     * @param int $isQueue
     * @return array
     * @throws Exception
     * @throws DbException
     */
    public function dayExport(array $chooseFields, array $file, array $params, int $isQueue = 0): array
    {
        $this->requestFields = array_flip((new ListService())->getApiFields());

        $fields = 'far.type,far.pay_fee,far.channel_order_amount,far.channel_commission,far.fba_shipping_fee,far.erp_shipped_refund_amount,far.erp_not_ship_refund_amount,
        far.refund_commission,far.erp_shipped_amount,far.internet_sales_tax_diff,sum(far.zhichi_channel_order_amount) AS zhichi_channel_order_amount,sum(far.zhichi_channel_order_amount_commission) as zhichi_channel_order_amount_commission,
        sum(far.advertising_cost) as advertising_cost,sum(far.credit_card_invest_amount) as credit_card_invest_amount,sum(far.shop_charges) as shop_charges,sum(far.unknown_fee) as unknown_fee';
        $daysReportSummary = $this->getQuery($params)
            ->group('far.account_shop_id,far.type')
            ->column($fields);
        $pageSize = 50000; // 每次最多执行5000条数据
        $count = (new ListService())->getCount($params);
        $num = ceil($count / $pageSize);
        $header = array_values($this->daysExportField($chooseFields));
        // $generator = $this->exportBuilder($num, $params, $pageSize);

        for ($i = 1; $i <= $num; $i++) {
            $offset = ($i - 1) * $pageSize;
            $accountIds = $this->getIds($params, $offset, $pageSize);
            $this->requestParams = $params;

            $this->queryDataByIds($accountIds);
            $generator = $this->restructData();
            /*$generator = $this->getQuery($params)
                ->group('far.account_shop_id,far.type')
                ->page($i, $pageSize)
                ->select();*/

            $accountIds = array_column($generator, 'account_shop_id');
            foreach ($accountIds as &$item) {
                $item = $item % OrderType::ChannelVirtual;
            }
            // 获取冻结时间 解冻时间
            $times = $this->getTimes($params['start_time'], $params['end_time']);
            $this->accountFrozenData = $accountFrozenData = (new AmazonFinaceExternal())->accountFrozen($accountIds, $times['startTime'], $times['endTime']);

            /**
             * 运营公司
             */
            $accountShopIds = array_unique(array_column($generator, 'account_shop_id'));
            if (!empty($accountShopIds)) {
                $dimAccountShopData = (new DimAccountShopModel())->where('id', 'in', $accountShopIds)
                    ->field('id,status as account_status,source_id,disable_time,site_code,sales_company_id')->select();
                $this->dimAccountShopData = array_column($dimAccountShopData, null, 'id');
            }

            /**
             * 销售员信息
             */
            if (!empty($accountIds)) {
                $accountUserMap = Db::table('channel_user_account_map')
                    ->where('account_id', 'in', $accountIds)
                    ->where('channel_id', DimChannelModel::CHANNEL_AMAZON)
                    ->field(['seller_id', '(account_id+200000) as account_shop_id'])
                    ->select();
                $this->accountUserMap = array_column($accountUserMap, 'seller_id', 'account_shop_id');
            }
            $sellerIds = array_unique(array_values($this->accountUserMap));
            if (!empty($sellerIds)) {
                $userData = Db::table('user')->where('id', 'in', $sellerIds)->field(['id', 'realname'])->select();
                $this->userData = array_column($userData, 'realname', 'id');
            }

            /**
             * 运营公司
             */
            $salesCompanyId = array_unique(array_column($this->dimAccountShopData, 'sales_company_id'));
            $salesCompanyData = (new Department())->getDepartmentInfoByDeptId($salesCompanyId, 'name,id');
            $this->salesCompanyData = array_column($salesCompanyData, 'name', 'id');
            /**
             * 拆分sub_data 为一维数组
             */
            $temp = [];
            $singleTemp = [];
            foreach ($generator as $key => $singleAccount) {
                $singleTemp = $singleAccount;
                // 共用的代码不方便获取id字段&&id仅用作合并单元格区分之用暂用此方式.
                $singleTemp['id'] = rand(1, 10000000000000000);
                foreach ($singleAccount['sub_data'] as $warehouse) {
                    $singleTemp['pay_fee'] = $warehouse['pay_fee'];
                    $singleTemp['channel_order_amount'] = floatval($warehouse['channel_order_amount']);
                    $singleTemp['channel_commission'] = $warehouse['channel_commission'];
                    $singleTemp['fba_shipping_fee'] = $warehouse['fba_shipping_fee'];
                    $singleTemp['erp_shipped_refund_amount'] = $warehouse['erp_shipped_refund_amount'];
                    $singleTemp['erp_not_ship_refund_amount'] = $warehouse['erp_not_ship_refund_amount'];
                    $singleTemp['refund_commission'] = $warehouse['refund_commission'];
                    $singleTemp['erp_shipped_amount'] = $warehouse['erp_shipped_amount'];
                    $singleTemp['erp_first_fee_and_tariff_cny'] = $warehouse['erp_first_fee_and_tariff_cny'];
                    $singleTemp['internet_sales_tax_diff'] = $warehouse['internet_sales_tax_diff'];
                    $singleTemp['type_text'] = $warehouse['type_text'];
                    $singleTemp['currency_code'] = $warehouse['currency_code'];

                    $temp[] = $singleTemp;
                }
            }
            $generator = $temp;

            $lists = [];
            $list = $this->formatDaysData($generator, $daysReportSummary);
            $lists = array_merge($lists, $list);
            return AmazonReceivablesExcel::exportExcel2007($header, $lists, $file, $params, $isQueue);
        }
    }

    /**
     * 格式化数据
     * @param array $data
     * @param array $summary
     * @return array
     */
    private function formatDaysData(array $data, array $summary)
    {
        $result = [];
        /*foreach ($data as $key => $item) {
            $accountId = $item['account_shop_id'] % OrderType::ChannelVirtual;
            $item['type_text'] = FactAmazonReceivablesTwoModel::TYPE_TXT[$item['type']] ?? '';
            // 店铺信息
            $accountShop = $this->dimAccountShopData[$item['account_shop_id']] ?? [];
            // 冻结时间 解冻时间
            $accountFrozenData = $this->accountFrozenData[$item['account_shop_id'] % OrderType::ChannelVirtual] ?? [];
            $item['frozen_time'] = $accountFrozenData['frozen_time'] ?? '--';
            $item['recover_time'] = $accountFrozenData['recover_time'] ?? '--';

            //店铺状态
            $accountStatus = (new ChannelAccountService())->getAccountStatusByAccountId(2, [$accountId]);
            $item['account_status_text'] = $accountStatus[$accountId] ?? '--';

            $sellerId = $this->accountUserMap[$item['account_shop_id']] ?? 0;
            $item['seller_name'] = $this->userData[$sellerId] ?? '';
            // 运营公司
            $salesCompanyId = $accountShop['sales_company_id'] ?? 0;
            $item['sales_company_name'] = $this->salesCompanyData[$salesCompanyId] ?? '';
            // $temp = [
            //     'id' => 'PO' . $datum['id'],
            //     'area' => '无',
            //     'warehouse' => '',
            //     'supplier' => '',
            // ];
            // $datum = $datum->toArray();
            // $purchaserId = $datum['purchase_user_id'];
            // $userInfo = Cache::store('User')->getOneUser($datum['purchase_user_id']);
            // $temp['purchaser'] = isset($userInfo['realname']) ? $userInfo['realname'] : '';
            // $temp['job_number'] = isset($userInfo['job_number']) ? $userInfo['job_number'] : 0;
            // $temp['create_date'] = date('Y-m-d H:i:s', $datum['create_time']);
            // $temp['discount_amount'] = $this->priceFormat($datum['discount_amount']);
            // $warehouse = Cache::store('warehouse')->getWarehouse($datum['warehouse_id']);
            // if (isset($warehouse['name'])) {
            //     $temp['warehouse'] = $warehouse['name'];
            // }
            // $area = PurchaseDiscountTargetModel::where('purchaser_id', $purchaserId)->value('area', '-1');
            // $temp['area'] = in_array($area, [0, 1], true) ? PurchaseDiscountTargetModel::getAreaTextAttr($area) : '无';
            // $supplier = Cache::store('supplier')->getSupplier($datum['supplier_id']);
            // if ($supplier) {
            //     $temp['supplier'] = $supplier['company_name'];
            // }
            // if (isset($summary[$purchaserId]['total_discount_amount'])) {
            //     $temp['total_discount_amount'] = $summary[$purchaserId]['total_discount_amount'];
            // }
            // $temp['status'] = PurchaserOrderModel::STATUS[$datum['status']];
            // $price = PurchaseOrderDetailModel::where('purchase_order_id', $datum['id'])->sum('price*qty');
            // $temp['payment'] = $this->priceFormat($price);
            // $temp['shipping_cost'] = $this->priceFormat($datum['shipping_cost']);
            // $temp['payable'] = $this->priceFormat(($price + $datum['shipping_cost'] - $datum['discount_amount']));
            $result[$key] = $item;
        }*/
        return $data;

        // $result = [];
        // foreach ($data as $datum) {
        //     $temp = [
        //         'id' => 'PO' . $datum['id'],
        //         'area' => '无',
        //         'warehouse' => '',
        //         'supplier' => '',
        //     ];
        //     $datum = $datum->toArray();
        //     $purchaserId = $datum['purchase_user_id'];
        //     $userInfo = Cache::store('User')->getOneUser($datum['purchase_user_id']);
        //     $temp['purchaser'] = isset($userInfo['realname']) ? $userInfo['realname'] : '';
        //     $temp['job_number'] = isset($userInfo['job_number']) ? $userInfo['job_number'] : 0;
        //     $temp['create_date'] = date('Y-m-d H:i:s', $datum['create_time']);
        //     $temp['discount_amount'] = $this->priceFormat($datum['discount_amount']);
        //     $warehouse = Cache::store('warehouse')->getWarehouse($datum['warehouse_id']);
        //     if (isset($warehouse['name'])) {
        //         $temp['warehouse'] = $warehouse['name'];
        //     }
        //     $area = PurchaseDiscountTargetModel::where('purchaser_id', $purchaserId)->value('area', '-1');
        //     $temp['area'] = in_array($area, [0, 1], true) ? PurchaseDiscountTargetModel::getAreaTextAttr($area) : '无';
        //     $supplier = Cache::store('supplier')->getSupplier($datum['supplier_id']);
        //     if ($supplier) {
        //         $temp['supplier'] = $supplier['company_name'];
        //     }
        //     if (isset($summary[$purchaserId]['total_discount_amount'])) {
        //         $temp['total_discount_amount'] = $summary[$purchaserId]['total_discount_amount'];
        //     }
        //     $temp['status'] = PurchaserOrderModel::STATUS[$datum['status']];
        //     $price = PurchaseOrderDetailModel::where('purchase_order_id', $datum['id'])->sum('price*qty');
        //     $temp['payment'] = $this->priceFormat($price);
        //     $temp['shipping_cost'] = $this->priceFormat($datum['shipping_cost']);
        //     $temp['payable'] = $this->priceFormat(($price + $datum['shipping_cost'] - $datum['discount_amount']));
        //     $result[] = $temp;
        // }
        // return $result;
        // return $data;
    }

    /**
     * 日报表导出字段
     * @param  $chooseField
     * @return array
     */
    public function daysExportField(array $chooseField = []): array
    {
        /**
         * type 所支持的类型参考：\XLSXWriter::numberFormatStandardized
         */
        $fields = [
            'account_code' => ['title' => '账号简称', 'key' => 'account_code', 'width' => 20, 'need_merge' => 1],
            'site_code' => ['title' => '站点', 'key' => 'site_code', 'width' => 20, 'need_merge' => 1],
            'seller_name' => ['title' => '销售员', 'key' => 'seller_name', 'width' => 20, 'need_merge' => 1],
            'account_status_text' => ['title' => '账号状态', 'key' => 'account_status_text', 'width' => 20, 'need_merge' => 1],
            'frozen_time' => ['title' => '冻结时间', 'key' => 'frozen_time', 'width' => 20, 'need_merge' => 1],
            'recover_time' => ['title' => '解冻时间', 'key' => 'recover_time', 'width' => 20, 'need_merge' => 1],
            'currency_code' => ['title' => '币种', 'key' => 'currency_code', 'width' => 20, 'need_merge' => 1],
            'type_text' => ['title' => '仓库类型', 'key' => 'type_text', 'width' => 20, 'need_merge' => 0],
            'pay_fee' => ['title' => '订单支付金额', 'key' => 'pay_fee', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'channel_order_amount' => ['title' => '平台订单金额', 'key' => 'channel_order_amount', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'channel_commission' => ['title' => '平台销售佣金', 'key' => 'channel_commission', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'fba_shipping_fee' => ['title' => 'FBA运费', 'key' => 'fba_shipping_fee', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'erp_shipped_refund_amount' => ['title' => '平台退款金额-已发货', 'key' => 'erp_shipped_refund_amount', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'erp_not_ship_refund_amount' => ['title' => '平台退款金额-未发货', 'key' => 'erp_not_ship_refund_amount', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'refund_commission' => ['title' => '退款返佣金', 'key' => 'refund_commission', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'erp_shipped_amount' => ['title' => '本期ERP发货订单金额', 'key' => 'erp_shipped_amount', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'internet_sales_tax_diff' => ['title' => '互联网销售税差异', 'key' => 'internet_sales_tax_diff', 'width' => 20, 'need_merge' => 0, 'type' => 'price'],
            'zhichi_channel_order_amount' => ['title' => '智持订单平台订单金额', 'key' => 'zhichi_channel_order_amount', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'zhichi_channel_order_amount_commission' => ['title' => '智持订单平台销售佣金', 'key' => 'zhichi_channel_order_amount_commission', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'advertising_cost' => ['title' => '广告费用', 'key' => 'advertising_cost', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'credit_card_invest_amount' => ['title' => '信用卡充值金额', 'key' => 'credit_card_invest_amount', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'shop_charges' => ['title' => '店铺费用', 'key' => 'shop_charges', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'unknown_fee' => ['title' => '调整费用', 'key' => 'unknown_fee', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'transfer_diff' => ['title' => '转账补差异', 'key' => 'transfer_diff', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'settled_transfer_amount' => ['title' => '已结算转账金额', 'key' => 'settled_transfer_amount', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'transfer_failed' => ['title' => '转账失败金额原币', 'key' => 'transfer_failed', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'opening_advance_balance' => ['title' => '期初预收账款余额', 'key' => 'opening_advance_balance', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'half_year_unshipped_order_price' => ['title' => '半年未发货订单售价', 'key' => 'half_year_unshipped_order_price', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'final_advance_balance' => ['title' => '期末预收账款余额', 'key' => 'final_advance_balance', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'final_advance_balance_check' => ['title' => '期末预收账款余额（校验）', 'key' => 'final_advance_balance_check', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'opening_receivables_balance' => ['title' => '期初应收账款余额', 'key' => 'opening_receivables_balance', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'final_receivables_balance' => ['title' => '期末应收账款余额', 'key' => 'final_receivables_balance', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            // 'multi_pay_amount' => ['title' => '已匹配原单的多渠道发货订单金额'],
            'final_reserve_amount' => ['title' => '期末预留金额', 'key' => 'final_reserve_amount', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'not_settlement_amount' => ['title' => '未结算金额', 'key' => 'not_settlement_amount', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'final_receivables_balance_check' => ['title' => '期末应收账款余额（校验）', 'key' => 'final_receivables_balance_check', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'opening_receivables_in_transit' => ['title' => '期初应收在途', 'key' => 'opening_receivables_in_transit', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'third_party_amount' => ['title' => '第三方入账金额', 'key' => 'third_party_amount', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'final_receivables_in_transit' => ['title' => '期末应收在途', 'key' => 'final_receivables_in_transit', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'final_receivables_in_transit_check' => ['title' => '期末应收在途（校验）', 'key' => 'final_receivables_in_transit_check', 'width' => 20, 'need_merge' => 1, 'type' => 'price'],
            'sales_company_name' => ['title' => '运营公司', 'key' => 'sales_company_name', 'width' => 20, 'need_merge' => 1],
        ];
        $result = [];
        if (empty($chooseField) && count($chooseField) == 0) {
            return $fields;
        }
        // foreach ($chooseField as $key => $field) {
        //     if (isset($fields[$field['field_key']])) {
        //         $result[$field['field_key']] = $fields[$field['field_key']];
        //         if ($title = param($field, 'field_name')) {
        //             $result[$field['field_key']]['title'] = $title;
        //         }
        //     }
        // }
        foreach ($chooseField as $key => $field) {
            if (isset($fields[$field])) {
                $result[$field] = $fields[$field];
                // if ($title = param($field, 'field_name')) {
                //     $result[$field['field_key']]['title'] = $title;
                // }
            }
        }
        return $result;
    }

    /**
     * 构建导出生成器, 减少内存使用.
     * @param float $num
     * @param int   $pageSize
     * @return \Generator
     */
    public function exportBuilder(float $num, $params, int $pageSize = 500)
    {
        // for ($i = 1; $i <= $num; $i++) {
        //     yield PurchaserOrderModel::alias('o')
        //         ->field("o.id,o.purchase_user_id,o.warehouse_id,o.supplier_id,o.status,o.discount_amount,o.create_time,o.shipping_cost")
        //         ->where($this->where)
        //         ->where('o.status', 'NOT IN', '-11, -1')
        //         ->page($i, $pageSize)
        //         ->select();
        // }
        for ($i = 1; $i <= $num; $i++) {
            yield $this->getQuery($params)
                ->group('far.account_shop_id,far.type')
                ->page(1, 500)
                ->select();
            // yield PurchaserOrderModel::alias('o')
            //     ->field("o.id,o.purchase_user_id,o.warehouse_id,o.supplier_id,o.status,o.discount_amount,o.create_time,o.shipping_cost")
            //     ->where($this->where)
            //     ->where('o.status', 'NOT IN', '-11, -1')
            //     ->page($i, $pageSize)
            //     ->select();
        }
    }

    /**
     * @title 构造查询对象
     * @param $params
     * @return FactDayAccountSummaryModel
     * @throws Exception
     */
    public function getQuery($params)
    {
        $accountShopIdWhere = [];
        $query = $this->getModel()
            ->alias('far')
            ->join('dim_account_shop das', 'das.id=far.account_shop_id', 'left');
        if (!empty($params['account_id'])) {
            $query->where('das.source_id', $params['account_id']);
        }
        // if ($params['account_status'] !== '') {
        //     $query->where('das.status', $params['account_status']);
        // }
        if ($params['account_status'] !== '') {
            $accountSite = new AccountStatusService();
            $query->join(CommonHelper::getDataBaseName(Config::get('collect_env'))['erp']
                         . '.amazon_account aa', 'das.source_id = aa.id', 'left');
            $query->join(CommonHelper::getDataBaseName(Config::get('collect_env'))['erp']
                         . '.account_site as', 'aa.code = as.account_code', 'left');
            /**
             * 参考：\app\index\service\AccountSite::accountStatus
             * 非“申诉中-账号部”、“无法申诉”这两种状态的时候需要排除掉
             */
            if (!in_array($params['account_status'], $accountSite::FROZEN_STATUS)) {
                $query->where('as.site_status', $params['account_status']);
                $query->where('as.frozen_status', 'not in', $accountSite::FROZEN_STATUS);
            }

            /**
             * 参考：\app\index\service\AccountSite::accountStatus
             * “申诉中-账号部”、“无法申诉”这两种状态的时候需要排除掉冻结状态的
             */
            if (in_array($params['account_status'], $accountSite::FROZEN_STATUS)) {
                $query->where('as.frozen_status', $params['account_status']);
                $baseAccountIds = (new \app\common\model\Account())->where('lock_status', $accountSite::ACCOUNT_STATUS_LOCKED)->column('id');
                $query->where('aa.id', 'not in', $baseAccountIds);
            }
        }

        //start_time,end_time
        if (!empty($params['start_time']) && !empty($params['end_time'])) {
            $periods = $this->getPeriods($params['start_time'], $params['end_time'], 'month');
            $query->where('far.period', 'in', $periods);
        }

        //seller_id
        if (!empty($params['seller_id'])) {
            $accountShopIds = Db::table('channel_user_account_map')->where('seller_id', $params['seller_id'])
                ->where('channel_id', DimChannelModel::CHANNEL_AMAZON)
                ->column('(account_id + 200000) as account_shop_id');
            $accountShopIdWhere[] = ['in', $accountShopIds];
        }

        if (!empty($params['account_ids'])) {
            $query->where('das.source_id', 'in', $params['account_ids']);
        }

        if (!CommonHelper::isCli()) {
            $userId = Common::getUserId();
        } else {
            if (!empty($params['apply_id'])) {
                $userId = Db::table('report_export_files')
                    ->where('id', $params['apply_id'])->value('applicant_id');
            }
        }

        $companyIds = CommonHelper::getSaleCompanyIds($userId);
        $query->where('das.sales_company_id', 'in', $companyIds);

        //账号过滤
        // $channelAccounts = $this->getChannelAccounts($userId);
        // if (!empty($channelAccounts)) {
        //     $accountShopIdWhere[] = ['in', $channelAccounts];
        // }
        // 账号简称
        if (!empty($params['account_codes'])) {
            $query->where('das.account_code', 'in', explode(',', $params['account_codes']));
        }

        if (!empty($accountShopIdWhere)) {
            $accountShopIdWhere[] = ['>', '0']; //凑够2个以上条件
            $query->where(['far.account_shop_id' => $accountShopIdWhere]);
        }

        return $query;
    }

    /**
     * 获取用户有权限的账户ID
     * @param $params
     * @return array|mixed
     */
    protected function getAccountIdByFilter($params)
    {
        $userId = Common::getUserId();
        if (empty($userId) && isset($params['apply_id'])) {
            $userId = (new ReportExportFiles())
                ->where(['id' => $params['apply_id']])
                ->value('applicant_id');
        }
        $accountIds = [];
        if (empty($userId)) {
            return $accountIds;
        }
        $object = new Filter(
            $this->getFilter(),
            false,
            $this->listController,
            $this->listMethod
        );
        $object->setUserId($userId);
        if ($object->filterIsEffective()) {
            $accountIds = $object->getFilterContent();
        }

        return $accountIds;
    }

    private function getModel()
    {
        $tableName = OrderHelper::getPeriodTableName('amazon_receivables_two', 'month');
        $model = new FactAmazonReceivablesTwoModel();
        $model->setTable($tableName);

        return $model;
    }

    public function getAmazonReceivablesCount($params)
    {
        return $this->getQuery($params)->group('far.account_shop_id,far.type')->count();
    }

    public function getPeriods($startTime, $endTime, $periodType)
    {
        $periods = [];
        if ($periodType == 'month') {
            $nextPeriodTimeStamp = strtotime($startTime);
            $endTimeTimeStamp = strtotime($endTime);
            while ($nextPeriodTimeStamp <= $endTimeTimeStamp) {
                $periods[] = date('Y-m', $nextPeriodTimeStamp);
                $nextPeriodTimeStamp = strtotime("+1months", $nextPeriodTimeStamp);
            }
        }

        return $periods;
    }

    /**
     * 转换单价格式, 保留几位
     * @param mixed $price
     * @param int   $decimals
     * @return string
     */
    private function priceFormat($price, int $decimals = 3): string
    {
        return number_format((float)$price, $decimals, '.', '');
    }

    private function getTimes($startTime, $endTime)
    {
        $startTime = strtotime($startTime);
        $endTime = strtotime('+1months', strtotime($endTime)) - 1 ;
        return [
            'startTime' => $startTime,
            'endTime' => $endTime,
        ];
    }

    public function rebuildData($accountData, $warehouseTypeData)
    {
        $data = [];
        foreach ($accountData as $item) {
            $accountShopId = $item['account_shop_id'];
            $item['fba'] = $this->calDetail([], []);
            $item['local'] = $this->calDetail([], []);
            $item['overseas'] = $this->calDetail([], []);
            $item['not_ship'] = $this->calDetail([], []);
            $item['fen_xiao'] = $this->calDetail([], []);
            $item['sub_total'] = $this->calDetail([], []);
            $currencyCode = $this->getCurrencyCode($this->requestParams, $item);
            $item = $this->getSomeFields($item);
            // echo json_encode($item);die;

            foreach ($warehouseTypeData as $typeItem) {
                if ($accountShopId != $typeItem['account_shop_id']) {
                    continue;
                }

                if ($typeItem['type'] == FactAmazonReceivablesTwoModel::TYPE_FBA) {
                    $item['fba'] = $this->calDetail($item['fba'], $typeItem);
                } elseif ($typeItem['type'] == FactAmazonReceivablesTwoModel::TYPE_LOCAL) {
                    $item['local'] = $this->calDetail($item['local'], $typeItem);
                } elseif ($typeItem['type'] == FactAmazonReceivablesTwoModel::TYPE_OVERSEAS) {
                    $item['overseas'] = $this->calDetail($item['overseas'], $typeItem);
                } elseif ($typeItem['type'] == FactAmazonReceivablesTwoModel::TYPE_NOT_SHIP) {
                    $item['not_ship'] = $this->calDetail($item['not_ship'], $typeItem);
                } elseif ($typeItem['type'] == FactAmazonReceivablesTwoModel::TYPE_FEN_XIAO) {
                    $item['fen_xiao'] = $this->calDetail($item['fen_xiao'], $typeItem);
                }
            }

            $item['sub_total'] = $this->getSubTotal($item['fba'], $item['local'], $item['overseas'], $item['not_ship'], $item['fen_xiao']);
            // echo json_encode($item);die;
            $newItem = [];
            foreach ($this->requestFields as $k => $v) {
                if (isset($this->detailFlipFields[$k])) {
                    continue;
                }
                $newItem[$k] = $item[$k];
            }

            $fba = [];
            $local = [];
            $overseas = [];
            $notShip = [];
            $fenXiao = [];
            $subTotal = [];
            $subData = [];
            foreach ($this->requestFields as $k => $v) {
                // echo '$this->requestFields' . json_encode($this->requestFields);die;
                if (!isset($this->detailFlipFields[$k])) {
                    continue;
                }
                $fba[$k] = $item['fba'][$k];
                $local[$k] = $item['local'][$k];
                $overseas[$k] = $item['overseas'][$k];
                $notShip[$k] = $item['not_ship'][$k];
                $fenXiao[$k] = $item['fen_xiao'][$k];
                $subTotal[$k] = $item['sub_total'][$k];
            }
            $subTotal = $this->format($subTotal);
            $subTotal['type_text'] = '合计';
            $subTotal['currency_code'] = $currencyCode;
            if (array_sum($local) > 0) {
                $local = $this->format($local);
                $local['type_text'] = '本地仓';
                $local['currency_code'] = $currencyCode;
                $subData['local'] = $local;
            }
            if (array_sum($overseas) > 0) {
                $overseas = $this->format($overseas);
                $overseas['type_text'] = '海外仓';
                $overseas['currency_code'] = $currencyCode;
                $subData['overseas'] = $overseas;
            }
            if (array_sum($fba) > 0) {
                $fba = $this->format($fba);
                $fba['type_text'] = 'FBA仓';
                $fba['currency_code'] = $currencyCode;
                $subData['fba'] = $fba;
            }
            if (array_sum($fenXiao) > 0) {
                $fenXiao = $this->format($fenXiao);
                $fenXiao['type_text'] = '分销仓';
                $fenXiao['currency_code'] = $currencyCode;
                $subData['fen_xiao'] = $fenXiao;
            }
            if (array_sum($notShip) > 0) {
                $notShip = $this->format($notShip);
                $notShip['type_text'] = '未发货';
                $notShip['currency_code'] = $currencyCode;
                $subData['not_ship'] = $notShip;
            }
            $subData['sub_total'] = $subTotal;
            $newItem['sub_data'] = array_values($subData);
            $newItem['sub_data'] = $subData;
            $newItem = $this->format($newItem);
            $data[] = $newItem;
        }
        // echo 'data' . json_encode($data);die;
        return $data;
    }

    private function calDetail($oldItem, $detailItem)
    {
        foreach ($this->detailFlipFields as $field => $v) {
            $oldItem[$field] = $oldItem[$field] ?? 0;
            $oldItem[$field] += ($detailItem[$field] ?? 0);
        }

        return $oldItem;
    }
}
