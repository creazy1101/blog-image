<?php

declare(strict_types = 1);
namespace app\report\service;

use app\common\service\Excel;
use app\common\service\ExcelNew;
use app\report\queue\PurchaseDiscountReportExportQueue;
use think\Exception;
use app\common\cache\Cache;
use app\common\service\Common as CommonService;
use app\common\model\PurchaseDiscountTargetModel;
use app\common\model\PurchaseOrder as PurchaserOrderModel;
use app\common\model\PurchaseOrderDetail as PurchaseOrderDetailModel;
use app\common\service\CommonQueuer;
use app\report\model\ReportExportFiles;


/**
 * Class PurchaseDiscountAmountReportService
 * @package app\report\service
 */
class PurchaseDiscountAmountReportService
{
    /**
     * 导出名字
     * @var array
     */
    private $exportName = [];

    /**
     * 查询条件
     * @var array
     */
    private $where = [];

    /**
     * 根据月份获取第一天和最后一天的日期
     * @param string $monthDate 月份('201901')
     * @return array
     */
    private function dateFormatByDay(string $monthDate): array
    {
        $year = substr($monthDate, 0, 4);
        $month = substr($monthDate, 4, 2);
        $lastDay = date('t', strtotime("{$year}-{$month}-01"));
        $lastDayOfMonth = "{$year}-{$month}-{$lastDay}";
        // 若今天不是月底最后一天, 只显示今天之前的采购数据
        if ($year === date('Y') && $month === date('m')) {
            if (strtotime(date('Y-m-d 23:59:59')) !== strtotime($lastDayOfMonth . '23:59:59')) {
                $today = date('d');
                $lastDayOfMonth = "{$year}-{$month}-{$today}";
            }
        }

        $firstDayOfMonth = "{$year}-{$month}-01";
        return ['start' => $firstDayOfMonth, 'end' => $lastDayOfMonth];
    }

    /**
     * 转换单价格式, 保留几位
     * @param mixed $price
     * @param int   $decimals
     * @return string
     */
    private function priceFormat($price, int $decimals = 3): string
    {
        return number_format((float)$price, $decimals, '.', '');
    }

    /**
     * 构建查询条件
     * @param array $params
     * @throws \think\Exception
     */
    public function getWhere(array $params)
    {
        if (empty($this->where)) {
            // 地区
            if (isParamNumber($params, 'area_id')) {
                $purchaserIds = PurchaseDiscountTargetModel::where('area', $params['area_id'])
                    ->column('DISTINCT purchaser_id');
                $this->where['o.purchase_user_id'] = ['IN', $purchaserIds];
                $this->exportName[] = $params['area_id'] == 0 ? '深圳' : '武汉';
            }
            // 采购员
            if (isParamNumber($params, 'purchaser_id')) {
                if (!empty($this->where['o.purchase_user_id'])) {
                    if (in_array($params['purchaser_id'], $this->where['o.purchase_user_id'])) {
                        $this->where['o.purchase_user_id'] = ['IN', $params['purchaser_id']];
                    } else {
                        $this->where['o.purchase_user_id'] = ['IN', 0];
                    }
                } else {
                    $this->where['o.purchase_user_id'] = ['IN', $params['purchaser_id']];
                }
                $this->exportName[] = Cache::store('User')->getOneUserRealname($params['purchaser_id']);
            }
            // 采购日期
            if (param($params, 'date_type')) {
                if ($params['date_type'] == 'days') {
                    $start = isset($params['start_time']) ? $params['start_time'] : '';
                    $end = isset($params['end_time']) ? $params['end_time'] : '';
                    $tempWhere = chooseDate($start, $end, 'o.create_time');
                    $this->exportName[] = '采购日期'. '：' . param($params, 'start_time') . '--' . param($params, 'end_time');
                } else {
                    $dateArr = $this->dateFormatByDay($params['month_date']);
                    $tempWhere = chooseDate($dateArr['start'], $dateArr['end'], 'o.create_time');
                    $this->exportName[] = '采购月份'. '：' . param($params, 'month_date');
                }
                $this->where = array_merge($this->where, $tempWhere);
            }
            // 采购员数组
            if (param($params, 'purchaser_ids')) {
                $purchaserIds = \json_decode($params['purchaser_ids']);
                $this->where['o.purchase_user_id'] = ['IN', $purchaserIds];
                $name = '采购员：';
                foreach ($purchaserIds as $purchaserId) {
                    $name .= Cache::store('User')->getOneUserRealname($purchaserId) . '_';
                }
                $this->exportName[] = rtrim($name, '_');
            }
            // 独立公司
            if (isParamNumber($params, 'sales_company_id')) {
                $this->where['o.sales_company_id'] = $params['sales_company_id'];
            }
        }
    }

    /**
     * 构建基本查询
     * @param array $params
     * @param string $group
     * @return mixed
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    private function getBaseQuery(array $params = [], string $group = 'o.purchase_user_id')
    {
        $this->getWhere($params);
        if (isset($params['date_type']) && $params['date_type'] === 'months') {
            $joinQuery = PurchaseDiscountTargetModel::field('purchaser_id, area, amount')
                ->where('date', $params['month_date'])
                ->buildSql();
            $field = 'o.purchase_user_id, t.amount, SUM(o.discount_amount) AS total_discount_amount, 
            IF (t.amount > 0, SUM(o.discount_amount) / t.amount, 1) AS progress';
            return PurchaserOrderModel::alias('o')
                ->field($field)
                ->join($joinQuery . ' t', 't.purchaser_id=o.purchase_user_id', 'LEFT')
                ->where($this->where)
                ->where('o.status', 'NOT IN', '-11, -1')
                ->group($group);
        }
        $joinQuery = PurchaseDiscountTargetModel::field('purchaser_id, area')
            ->group('purchaser_id')
            ->buildSql();
        return PurchaserOrderModel::alias('o')
            ->field('`o`.`purchase_user_id`,`t`.`area`,SUM(`o`.`discount_amount`) AS `total_discount_amount`')
            ->join($joinQuery . ' t', 't.purchaser_id=o.purchase_user_id', 'LEFT')
            ->where($this->where)
            ->where('o.status', 'NOT IN', '-11, -1')
            ->group($group);
    }

    /**
     * 获取总数
     * @param array $params
     * @return int
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    private function count(array $params): int
    {
        return $this->getBaseQuery($params)->count() ?? 0;
    }

    /**
     * 采购优惠日报表
     * @param array $params
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function getDayReportList(array $params): array
    {
        if (param($params,'sort_field')) {
            $data = $this->getBaseQuery($params)
                ->page($params['page'], $params['pageSize'])
                ->order($params['sort_field'], $params['sort_type'])
                ->select();
        } else {
            $data = $this->getBaseQuery($params)
                ->page($params['page'], $params['pageSize'])
                ->select();
        }
        $list = [];
        foreach ($data as $datum) {
            $userInfo = Cache::store('User')->getOneUser($datum['purchase_user_id']);
            $list[] = [
                'area' => $datum['area'] !== null ? PurchaseDiscountTargetModel::getAreaTextAttr($datum['area']): '无',
                'purchaser_id' => $datum['purchase_user_id'],
                'purchaser' => isset($userInfo['realname']) ? $userInfo['realname'] : '',
                'job_number' => isset($userInfo['job_number']) ? $userInfo['job_number'] : 0,
                'total_discount_amount' => !empty($datum['total_discount_amount'])
                    ?  $this->priceFormat($datum['total_discount_amount']) : '0.000',
            ];
        }
        if (!isset($params['sort_field']) && !isset($params['sort_type'])) {
            array_multisort(array_column($list, 'job_number'), SORT_ASC, $list);
        }
        return ['list' => $list, 'count' => $this->count($params)];
    }

    /**
     * 获取详情条件
     * @param array $params
     * @return array
     */
    private function getDetailWhere(array $params): array
    {
        $where = [];
        if (isParamNumber($params, 'warehouse_id')) {
            $where['warehouse_id'] = ['EQ', $params['warehouse_id']];
        }
        if (isParamNumber($params, 'supplier_id')) {
            $where['supplier_id'] = ['EQ', $params['supplier_id']];
        }
        if (isParamNumber($params, 'status')) {
            $where['status'] = ['EQ', $params['status']];
        }
        $startTime = isset($params['start_time']) ? $params['start_time'] : '';
        $endTime = isset($params['end_time']) ? $params['end_time'] : '';
        $timeWhere = chooseDate($startTime, $endTime, 'create_time');
        $where = array_merge($where, $timeWhere);
        return $where;
    }

    /**
     * @param array $params
     * @return mixed
     */
    private function getDetailQuery(array $params)
    {
        $where = $this->getDetailWhere($params);
        return PurchaserOrderModel::field('id,warehouse_id,status,supplier_id,discount_amount,shipping_cost,create_time')
            ->where($where)
            ->where('purchase_user_id', $params['purchaser_id'])
            ->where('status', 'NOT IN', '-11, -1');
    }

    /**
     * 获取采购优惠总额详情
     * @param array $params
     * @return array
     * @throws \think\Exception
     */
    public function getDaysDetail(array $params): array
    {
        $data = $this->getDetailQuery($params)->page($params['page'], $params['pageSize'])->select();
        $ids = array_column($data, 'id');
        $paymentByPurchaseID = PurchaseOrderDetailModel::where('purchase_order_id', 'IN', $ids)
            ->group('purchase_order_id')
            ->column('SUM(qty*price) AS payment','purchase_order_id');
        $list = [];
        $count = $this->getDetailQuery($params)->count();
        foreach ($data as $datum) {
            $payable = 0;
            if (!empty($paymentByPurchaseID[$datum['id']])) {
                $payable = $paymentByPurchaseID[$datum['id']] + $datum['shipping_cost'] - $datum['discount_amount'];
            }
            $warehouseName = '';
            $warehouse = Cache::store('warehouse')->getWarehouse($datum['warehouse_id']);
            if (isset($warehouse['name'])) {
                $warehouseName = $warehouse['name'];
            }
            $supplierName = '';
            $supplier = Cache::store('supplier')->getSupplier($datum['supplier_id']);
            if ($supplier) {
                $supplierName = $supplier['company_name'];
            }
            $list[] = [
                'purchase_order_id' => 'PO' . $datum['id'],
                'warehouse' => $warehouseName,
                'status' => PurchaserOrderModel::STATUS[$datum['status']],
                'supplier' => $supplierName,
                'payment' => isset($paymentByPurchaseID[$datum['id']])
                    ? $this->priceFormat($paymentByPurchaseID[$datum['id']]) : '0.000',
                'payable' => $this->priceFormat($payable),
                'shipping_cost' => $this->priceFormat($datum['shipping_cost']),
                'discount_amount' => $this->priceFormat($datum['discount_amount']),
                'create_date' => date('Y-m-d H:i:s', $datum['create_time']),
            ];
        }
        return ['list' => $list, 'count' => $count];
    }

    /**
     * 导出文件名称
     * @param string $type
     * @return string
     */
    private function getExportFileName(string $type = 'days'): string
    {
        $fileType = $type === 'days' ? '日' : '月';
        $fileName = '采购优惠'. $fileType .'报表导出'.'('. $userId = CommonService::getUserInfo()->toArray()['realname'].')__'.date("Y_m_d_H_i_s");
        if (!empty($this->exportName)) {
            $fileName = implode('_', $this->exportName) . '_采购计划'. $fileType .'报表'.'('. $userId = CommonService::getUserInfo()->toArray()['realname'].')__'.date("Y_m_d_H_i_s");
        }
        return $fileName;
    }

    /**
     * 构建导出生成器, 减少内存使用.
     * @param float $num
     * @param int   $pageSize
     * @return \Generator
     */
    public function exportBuilder(float $num, int $pageSize = 500)
    {
        for ($i = 1; $i <= $num; $i++) {
            yield PurchaserOrderModel::alias('o')
                ->field("o.id,o.purchase_user_id,o.warehouse_id,o.supplier_id,o.status,o.discount_amount,o.create_time,o.shipping_cost")
                ->where($this->where)
                ->where('o.status', 'NOT IN', '-11, -1')
                ->page($i, $pageSize)
                ->select();
        }
    }

    /**
     * 日报表导出字段
     * @param  $chooseField
     * @return array
     */
    public function daysExportField(array $chooseField = []): array
    {
        $fields = [
            'area' => ['title' => '地区', 'key' => 'area', 'width' => 20, 'need_merge' => 1],
            'purchaser' => ['title' => '采购员', 'key' => 'purchaser', 'width' => 20, 'need_merge' => 1, 'main_need_merge_key' => 1],
            'job_number' => ['title' => '工号', 'key' => 'job_number', 'width' => 20, 'need_merge' => 1],
            'total_discount_amount' => ['title' => '优惠总额', 'key' => 'total_discount_amount', 'width' => 20, 'need_merge' => 1],
            'id' => ['title' => '采购号', 'key' => 'id', 'width' => 10, 'need_merge' => 0],
            'warehouse' => ['title' => '采购仓库', 'key' => 'warehouse', 'width' => 10, 'need_merge' => 0],
            'status' => ['title' => '采购状态', 'key' => 'status', 'width' => 10, 'need_merge' => 0],
            'supplier' => ['title' => '供应商', 'key' => 'supplier', 'width' => 10, 'need_merge' => 0],
            'payment' => ['title' => '货款', 'key' => 'payment', 'width' => 10, 'need_merge' => 0],
            'shipping_cost' => ['title' => '运费', 'key' => 'shipping_cost', 'width' => 10, 'need_merge' => 0],
            'payable' => ['title' => '应付款', 'key' => 'payable', 'width' => 10, 'need_merge' => 0],
            'discount_amount' => ['title' => '优惠金额', 'key' => 'discount_amount', 'width' => 10, 'need_merge' => 0],
            'create_date' => ['title' => '采购日期', 'key' => 'create_date', 'width' => 20, 'need_merge' => 0],
        ];
        $result = [];
        if(empty($chooseField) && count($chooseField) == 0) {
            return $fields;
        }
        foreach($chooseField as $key=>$field) {
            if(isset($fields[$field['field_key']])) {
                $result[$field['field_key']] = $fields[$field['field_key']];
                if($title = param($field, 'field_name')) {
                    $result[$field['field_key']]['title'] = $title;
                }
            }
        }
        return $result;
    }

    /**
     * 获取采购单总数
     * @return int
     */
    private function getPurchaserOrderCount(): int
    {
        return PurchaserOrderModel::alias('o')->where($this->where)
                ->where('o.status', 'NOT IN', '-11, -1')
                ->join('purchase_discount_target t', 'o.purchase_user_id=t.purchaser_id', 'LEFT')
                ->count() ?? 0;
    }

    /**
     * 执行导出
     * @param array $file
     * @param array $chooseFields
     * @param int   $isQueue
     * @return array
     * @throws Exception
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \think\exception\DbException
     */
    public function dayExport(array $chooseFields, array $file, int $isQueue = 0): array
    {
        $joinQuery = PurchaseDiscountTargetModel::field('purchaser_id, area')
            ->group('purchaser_id')
            ->buildSql();
        $daysReportSummary = PurchaserOrderModel::alias('o')
            ->join($joinQuery . ' t', 't.purchaser_id=o.purchase_user_id', 'LEFT')
            ->where($this->where)
            ->where('o.status', 'NOT IN', '-11, -1')
            ->group('o.purchase_user_id')
            ->column('t.area,SUM(o.discount_amount) AS total_discount_amount', 'o.purchase_user_id');
        $pageSize = 5000; //每次最多执行5000条数据
        $count = $this->getPurchaserOrderCount();
        $num = ceil($count/$pageSize);
        $header = array_values($this->daysExportField($chooseFields));
        $generator = $this->exportBuilder($num, $pageSize);
        if ($isQueue && $count > 20000) {
            $excelClass = new ExcelNew();
            $excelClass->setXlsxRowMax(200000);//每个文件最大导出行数
            $excelClass->setFileName($file);
            $excelClass->handlHeader($header);
            foreach ($generator as $item) {
                $list = $this->formatDaysData($item, $daysReportSummary);
                $excelClass->exportExcel2007($list);
            }
            $result = $excelClass->exportTofild($isQueue);
            return $result;
        } else {
            $lists = [];
            foreach ($generator as $item) {
                $list = $this->formatDaysData($item, $daysReportSummary);
                $lists = array_merge($lists, $list);
            }
            $result = Excel::exportExcel2007($header, $lists, $file, $isQueue);
            return $result;
        }
    }

    /**
     * 格式化数据
     * @param array $data
     * @param array $summary
     * @return array
     * @throws Exception
     */
    private function formatDaysData(array $data, array $summary)
    {
        $result = [];
        foreach ($data as $datum) {
            $temp = [
                'id' => 'PO'.$datum['id'],
                'area' => '无',
                'warehouse' => '',
                'supplier' => '',
            ];
            $datum = $datum->toArray();
            $purchaserId = $datum['purchase_user_id'];
            $userInfo = Cache::store('User')->getOneUser($datum['purchase_user_id']);
            $temp['purchaser'] = isset($userInfo['realname']) ? $userInfo['realname'] : '';
            $temp['job_number'] = isset($userInfo['job_number']) ? $userInfo['job_number'] : 0;
            $temp['create_date'] = date('Y-m-d H:i:s', $datum['create_time']);
            $temp['discount_amount'] = $this->priceFormat($datum['discount_amount']);
            $warehouse = Cache::store('warehouse')->getWarehouse($datum['warehouse_id']);
            if (isset($warehouse['name'])) {
                $temp['warehouse'] = $warehouse['name'];
            }
            $area = PurchaseDiscountTargetModel::where('purchaser_id', $purchaserId)->value('area', '-1');
            $temp['area'] = in_array($area, [0, 1], true) ? PurchaseDiscountTargetModel::getAreaTextAttr($area) : '无';
            $supplier = Cache::store('supplier')->getSupplier($datum['supplier_id']);
            if ($supplier) {
                $temp['supplier'] = $supplier['company_name'];
            }
            if (isset($summary[$purchaserId]['total_discount_amount'])) {
                $temp['total_discount_amount'] = $summary[$purchaserId]['total_discount_amount'];
            }
            $temp['status'] = PurchaserOrderModel::STATUS[$datum['status']];
            $price = PurchaseOrderDetailModel::where('purchase_order_id', $datum['id'])->sum('price*qty');
            $temp['payment'] = $this->priceFormat($price);
            $temp['shipping_cost'] = $this->priceFormat($datum['shipping_cost']);
            $temp['payable'] = $this->priceFormat(($price + $datum['shipping_cost'] - $datum['discount_amount']));
            $result[] = $temp;
        }
        return $result;
    }

    /**
     * 请求导出
     * @param array $params
     * @return array
     * @throws Exception
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \think\exception\DbException
     */
    public function doDaysExport(array $params): array
    {
        $this->getWhere($params);
        $count = $this->getPurchaserOrderCount();
        if ($count > 500) {
            $this->applyDaysExport($params, $count);
            return ['message'=> '申请成功', 'join_queue' => 1];
        } else {
            $file = [
                'name' => $this->getExportFileName(),
                'path' => 'purchase_order_export',
            ];
            $chooseField = \json_decode($params['fields'], true);
            return $this->dayExport($chooseField, $file);
        }
    }

    /**
     * 申请队列导出
     * @param array $params
     * @param int $count
     * @throws Exception
     */
    private function applyDaysExport(array $params, int $count)
    {
        try{
            $extension = '.xlsx';
            $userId = CommonService::getUserInfo()->toArray()['user_id'];
            if ($count > 20000) {
                $extension = '.zip';//超过20000行数据，分文件压缩导出
            }
            $fileName = $this->getExportFileName();
            $model = new ReportExportFiles();
            $model->applicant_id     = $userId;
            $model->apply_time       = time();
            $model->export_file_name = $fileName.$extension;
            $model->status =0;
            if (!$model->save()) {
                throw new Exception('导出请求创建失败',500);
            }
            $params['file'] = [
                'name' => $fileName,
                'path' => 'purchase_order_export',
            ];
            $params['export_report_type'] = 'days';
            $params['apply_id'] = $model->id;
            $queue = new CommonQueuer(PurchaseDiscountReportExportQueue::class);
            $queue->push($params);
        } catch (\Exception $ex) {
            Cache::handler()->hset(
                'hash:export_purchase_discount_report',
                'error_'.time(),
                $ex->getMessage());
            throw new Exception($ex->getMessage(),500);
        }
    }

    /**
     * 采购优惠月报表
     * @param array $params
     * @return array
     * @throws Exception
     * @throws \think\exception\DbException
     */
    public function getMonthReportList(array $params): array
    {
        $list = [];
        if (!param($params, 'sort_field')) {
            $monthData = $this->getBaseQuery($params)
                ->page($params['page'], $params['pageSize'])
                ->select();
        } else {
            $monthData = $this->getBaseQuery($params)
                ->page($params['page'], $params['pageSize'])
                ->order($params['sort_field'], $params['sort_type'])
                ->select();
        }
        foreach ($monthData as $monthDatum) {
            $userInfo = Cache::store('User')->getOneUser($monthDatum['purchase_user_id']);
            $area = '无';
            $queryArea = PurchaseDiscountTargetModel::where('purchaser_id', $monthDatum['purchase_user_id'])
                ->value('area');
            if (isset($queryArea) && in_array($queryArea, [0, 1], true)) {
                $area = PurchaseDiscountTargetModel::getAreaTextAttr($queryArea);
            }
            $list[] = [
                'area' => $area,
                'purchaser_id' => $monthDatum['purchase_user_id'],
                'purchaser' => isset($userInfo['realname']) ? $userInfo['realname'] : '',
                'job_number' => isset($userInfo['job_number']) ? $userInfo['job_number'] : 0,
                'amount' => !empty($monthDatum['amount']) ? $this->priceFormat($monthDatum['amount']) : '-- --',
                'total_discount_amount' => !empty($monthDatum['total_discount_amount'])
                    ?  $this->priceFormat($monthDatum['total_discount_amount']) : '0.000',
                'progress' => $this->priceFormat(((float)$monthDatum['progress'] * 100), 2),
                'date' => $params['month_date']
            ];
        }
        if (!isset($params['sort_field']) && !isset($params['sort_type'])) {
            array_multisort(array_column($list, 'job_number'), SORT_ASC, $list);
        }
        return ['list' => $list, 'count' => $this->count($params)];
    }

    /**
     * 根据月份获取日期汇总
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function getDaysDetailByMonth(array $params): array
    {
        $result = [];
        $daysByMonth = $this->dateFormatByDay($params['month_date']);
        $timeWhere = chooseDate($daysByMonth['start'], $daysByMonth['end'], 'create_time');
        $daysData = PurchaserOrderModel::field("FROM_UNIXTIME(create_time, '%Y-%m-%d') AS create_date,SUM(discount_amount) AS total_discount_amount")
            ->where([
                'status' => ['NOT IN', [-11, -1]],
                'purchase_user_id' => ['EQ', $params['purchaser_id']],
            ])->where($timeWhere)
            ->group('create_date')
            ->select();
        $formatDaysData = [];
        $totalPrice = 0;
        foreach ($daysData as $datum) {
            $formatDaysData[] = [
                'create_date' => $datum['create_date'],
                'total_discount_amount' => $this->priceFormat($datum['total_discount_amount'])
            ];
            $totalPrice += $datum['total_discount_amount'];
        }
        $hasDays = array_column($formatDaysData, 'create_date');
        $hasNotDays = [];
        for ($day = $daysByMonth['start']; $day <= $daysByMonth['end']; $day++) {
            if (in_array($day, $hasDays)) {
                continue;
            }
            $hasNotDays[] = ['create_date' => $day, 'total_discount_amount' => "0.000"];
        }
        $result['purchaser'] = Cache::store('User')->getOneUserRealname($params['purchaser_id']);
        $result['purchaser_id'] = $params['purchaser_id'];
        $result['month_total_discount_amount'] = $this->priceFormat($totalPrice);
        $list = array_merge($formatDaysData, $hasNotDays);
        array_multisort(array_column($list, 'create_date'), SORT_ASC, $list);
        $result['list'] = $list;
        return $result;
    }

    /**
     * 月报表导出字段
     * @return array
     */
    public function monthsExportField(array $chooseField = []): array
    {
        $result = [];
        $fields = [
            'area' => ['title' => '地区', 'key' => 'area', 'width' => 20, 'need_merge' => 1],
            'purchaser' => ['title' => '采购员', 'key' => 'purchaser', 'width' => 20, 'need_merge' => 1, 'main_need_merge_key' => 1],
            'job_number' => ['title' => '工号', 'key' => 'job_number', 'width' => 20, 'need_merge' => 1],
            'target_discount_amount' => ['title' => '优惠目标', 'key' => 'target_discount_amount', 'width' => 10, 'need_merge' => 1],
            'total_discount_amount' => ['title' => '优惠总额', 'key' => 'total_discount_amount', 'width' => 10, 'need_merge' => 1],
            'progress' => ['title' => '进度', 'key' => 'progress', 'width' => 10, 'need_merge' => 1],
            'date' => ['title' => '日期', 'key' => 'date', 'width' => 10, 'need_merge' => 1, 'merge_group' => 1],
            'discount_subtotal' => ['title' => '优惠小计', 'key' => 'discount_subtotal', 'width' => 10, 'need_merge' => 1, 'merge_group' => 1],
            'id' => ['title' => '采购单号', 'key' => 'id', 'width' => 10, 'need_merge' => 0],
            'warehouse' => ['title' => '仓库', 'key' => 'warehouse', 'width' => 10, 'need_merge' => 0],
            'status' => ['title' => '采购状态', 'key' => 'status', 'width' => 10, 'need_merge' => 0],
            'supplier' => ['title' => '供应商', 'key' => 'supplier', 'width' => 10, 'need_merge' => 0],
            'payment' => ['title' => '货款', 'key' => 'payment', 'width' => 10, 'need_merge' => 0],
            'shipping_cost' => ['title' => '运费', 'key' => 'shipping_cost', 'width' => 10, 'need_merge' => 0],
            'payable' => ['title' => '应付款', 'key' => 'payable', 'width' => 10, 'need_merge' => 0],
            'discount_amount' => ['title' => '优惠金额', 'key' => 'discount_amount', 'width' => 10, 'need_merge' => 0],
            'create_date' => ['title' => '采购日期', 'key' => 'create_date', 'width' => 20, 'need_merge' => 0],
        ];
        if(empty($chooseField) && count($chooseField) == 0) {
            return $fields;
        }
        foreach($chooseField as $key=>$field) {
            if(isset($fields[$field['field_key']])) {
                $result[$field['field_key']] = $fields[$field['field_key']];
                if($title = param($field, 'field_name')) {
                    $result[$field['field_key']]['title'] = $title;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $params
     * @return array
     * @throws \think\Exception
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \think\exception\DbException
     */
    public function doMonthsExport(array $params): array
    {
        $this->getWhere($params);
        $count = $this->getPurchaserOrderCount();
        if ($count > 500) {
            $this->applyMonthsExport($params, $count);
            return ['message'=> '申请成功', 'join_queue' => 1];
        } else {
            $file = [
                'name' => $this->getExportFileName('months'),
                'path' => 'purchase_order_export',
            ];
            $chooseField = \json_decode($params['fields'], true);
            return $this->monthsExport($chooseField, $params['month_date'], $file);
        }
    }

    /**
     * @param array $params
     * @param int $count
     * @throws \think\Exception
     */
    private function applyMonthsExport(array $params, int $count)
    {
        try {
            $extension = '.xlsx';
            $userId = CommonService::getUserInfo()->toArray()['user_id'];
            if ($count > 20000) {
                $extension = '.zip';//超过20000行数据，分文件压缩导出
            }
            $fileName = $this->getExportFileName('months');
            $model = new ReportExportFiles();
            $model->applicant_id     = $userId;
            $model->apply_time       = time();
            $model->export_file_name = $fileName.$extension;
            $model->status =0;
            if (!$model->save()) {
                throw new Exception('导出请求创建失败',500);
            }
            $params['file'] = [
                'name' => $fileName,
                'path' => 'purchase_order_export',
            ];
            $params['apply_id'] = $model->id;
            $params['export_report_type'] = 'month';
            $queue = new CommonQueuer(PurchaseDiscountReportExportQueue::class);
            $queue->push($params);
        } catch (Exception $ex) {
            Cache::handler()->hset(
                'hash:export_purchase_discount_report',
                'error_'.time(),
                $ex->getMessage());
            throw new Exception($ex->getMessage(),500);
        }
    }

    /**
     * 月报表导出
     * @param array   $chooseFields
     * @param array   $file
     * @param string  $monthDate
     * @param int     $isQueue
     * @return array
     * @throws Exception
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \think\exception\DbException
     */
    public function monthsExport(array $chooseFields, string $monthDate , array $file, int $isQueue = 0): array
    {
        $joinQuery = PurchaseDiscountTargetModel::field('purchaser_id, amount, area')
            ->where(['date' => $monthDate])
            ->group('purchaser_id')
            ->buildSql();
        $monthReportSummary = PurchaserOrderModel::alias('o')
            ->join($joinQuery . ' t', 't.purchaser_id=o.purchase_user_id', 'LEFT')
            ->where($this->where)
            ->where('o.status', 'NOT IN', '-11, -1')
            ->group('o.purchase_user_id')
            ->column('o.purchase_user_id,t.area,t.amount AS target_discount_amount,SUM(o.discount_amount) AS total_discount_amount,
            IF (t.amount > 0, SUM(o.discount_amount) / t.amount , 1) AS progress', 'o.purchase_user_id');
        $pageSize = 5000;
        $count = $this->getPurchaserOrderCount();
        $num = ceil($count / $pageSize);
        $header = array_values($this->monthsExportField($chooseFields));
        $generator = $this->exportBuilder($num, $pageSize);
        if ($isQueue && $count > 20000) {
            $excelClass = new ExcelNew();
            $excelClass->setXlsxRowMax(1048576);//每个文件最大导出行数
            $excelClass->setFileName($file);
            $excelClass->handlHeader($header);
            foreach ($generator as $item) {
                $list = $this->formatMonthData($item, $monthReportSummary, $monthDate);
                $excelClass->exportExcel2007($list);
            }
            $result = $excelClass->exportTofild($isQueue);
            return $result;
        } else {
            $lists = [];
            foreach ($generator as $item) {
                $list = $this->formatMonthData($item, $monthReportSummary, $monthDate);
                $lists = array_merge($lists, $list);
            }
            array_multisort(array_column($lists, 'job_number'), SORT_ASC, array_column($lists, 'date'), SORT_ASC, $lists);
            $result = Excel::exportExcel2007($header, $lists, $file, $isQueue);
            return $result;
        }
    }

    /**
     * 格式月报表数据
     * @param array $data
     * @param array $monthReportSummary
     * @param string $monthDate
     * @return array
     * @throws Exception
     */
    private function formatMonthData(array $data, array $monthReportSummary, string $monthDate): array
    {
        $result = $hasNotDays = $hasDaysData = [];
        $purchaserIds = array_unique(array_column($data, 'purchase_user_id'));
        $daysByMonth = $this->dateFormatByDay($monthDate);
        $timeWhere = chooseDate($daysByMonth['start'], $daysByMonth['end'], 'create_time');
        foreach ($purchaserIds as $id) {
            $daysData = PurchaserOrderModel::where([
                'status' => ['NOT IN', [-11, -1]],
                'purchase_user_id' => ['EQ', $id]
            ])->where($timeWhere)
                ->group("FROM_UNIXTIME(create_time, '%Y-%m-%d'), purchase_user_id")
                ->column("FROM_UNIXTIME(create_time, '%Y-%m-%d') AS create_date,SUM(discount_amount) AS total_discount_amount, purchase_user_id");
            $userInfo = Cache::store('User')->getOneUser($id);
            for ($day = $daysByMonth['start']; $day <= $daysByMonth['end']; $day++) {
                if (array_key_exists($day, $daysData)) {
                    $hasDaysData[$id][$day] = $daysData[$day]['total_discount_amount'];
                } else {
                    $hasNotDays[] = [
                        'area' => !empty($monthReportSummary[$id]['area']) ? $monthReportSummary[$id]['area'] : '无',
                        'purchaser' => isset($userInfo['realname']) ? $userInfo['realname'] : '',
                        'job_number' => isset($userInfo['job_number']) ? $userInfo['job_number'] : '',
                        'warehouse' => '',
                        'supplier' => '',
                        'target_discount_amount' => !empty($monthReportSummary[$id]['target_discount_amount'])
                            ? $monthReportSummary[$id]['target_discount_amount'] : '-- --',
                        'total_discount_amount' => $monthReportSummary[$id]['total_discount_amount'],
                        'progress' => ($monthReportSummary[$id]['progress'] * 100) . '%',
                        'discount_subtotal' => '0.0000',
                        'date' => $day,
                        'id' => '',
                        'create_date' => '',
                        'status' => '',
                        'discount_amount' => '',
                        'shipping_cost' => '',
                        'payment' => '',
                        'payable' => ''
                    ];
                }
            }
        }
        foreach ($data as $datum) {
            $temp = [
                'id' => 'PO'.$datum['id'],
                'area' => '无',
                'warehouse' => '',
                'supplier' => '',
            ];
            $userInfo = Cache::store('User')->getOneUser($datum['purchase_user_id']);
            // 月份汇总
            $temp['target_discount_amount'] = !empty($monthReportSummary[$datum['purchase_user_id']]['target_discount_amount'])
                ? $monthReportSummary[$id]['target_discount_amount'] : '-- --';
            $temp['total_discount_amount'] = $monthReportSummary[$datum['purchase_user_id']]['total_discount_amount'];
            $temp['progress'] = ($monthReportSummary[$id]['progress'] * 100) . '%';
            $date = date('Y-m-d', $datum['create_time']);
            $temp['create_date'] = date('Y-m-d H:i:s', $datum['create_time']);
            $temp['purchaser'] = isset($userInfo['realname']) ? $userInfo['realname'] : '';
            $temp['job_number'] = isset($userInfo['job_number']) ? $userInfo['job_number'] : '';
            $area = PurchaseDiscountTargetModel::where('purchaser_id', $datum['purchase_user_id'])->value('area', '-1');
            $temp['area'] = in_array($area, [0, 1], true) ? PurchaseDiscountTargetModel::getAreaTextAttr($area) : '无';
            $temp['discount_amount'] = $this->priceFormat($datum['discount_amount']);
            $warehouse = Cache::store('warehouse')->getWarehouse($datum['warehouse_id']);
            if (isset($warehouse['name'])) {
                $temp['warehouse'] = $warehouse['name'];
            }
            $supplier = Cache::store('supplier')->getSupplier($datum['supplier_id']);
            if ($supplier) {
                $temp['supplier'] = $supplier['company_name'];
            }
            // 日期汇总
            $temp['date'] = $date;
            $temp['discount_subtotal'] = !empty($hasDaysData) ? $hasDaysData[$datum['purchase_user_id']][$date] : '0.0000';
            $temp['status'] = PurchaserOrderModel::STATUS[$datum['status']];
            $price = PurchaseOrderDetailModel::where('purchase_order_id', $datum['id'])->sum('price*qty');
            $temp['payment'] = $this->priceFormat($price);
            $temp['shipping_cost'] = $this->priceFormat($datum['shipping_cost']);
            $temp['payable'] = $this->priceFormat(($price + $datum['shipping_cost'] - $datum['discount_amount']));
            $result[] = $temp;
        }
        $list = array_merge($result, $hasNotDays);
        return $list;
    }
}